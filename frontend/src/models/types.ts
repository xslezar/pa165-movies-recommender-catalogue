import { KeyedMutator } from 'swr/dist/types';

export enum MovieCardMode {
  LandingPage,
  Recommend
}

export interface UserAuth {
  exp: number,
  iat: number,
  iss: string,
  name: string,
  role: string,
  sub: number
}

export interface MovieDto {
  id: number,
  createdAt: Date,
  updatedAt: Date,
  name: string,
  duration: number,
  poster: string,
  genres: string[],
  description: string,
  releaseYear: number,
  reviews: SimpleReviewDto,
  actors: PersonDto[],
  director: PersonDto,
}

export interface ReviewDto {
  id: number,
  createdAt: Date,
  updatedAt: Date,
  user: SimplerUserDto,
  movie: SimpleMovieDto,
  text: string,
  scriptRating: number,
  ideaRating: number,
  visualsEditRating: number,
  musicRating: number,
  actingRating: number,
}

export interface PersonDto {
  id: number,
  createdAt: Date,
  updatedAt: Date,
  name: string,
  picture: string,
  birth: Date,
  about: string,
  actedInMovies: SimpleMovieDto,
  directedMovies: SimpleMovieDto,
}

export interface SimpleMovieDto {
  id: number,
  createdAt: Date,
  updatedAt: Date,
  name: string,
  duration: number,
  poster: string,
  releaseYear: number,
}

export interface SimpleReviewDto {
  id: number,
  createdAt: Date,
  updatedAt: Date,
  user: SimplerUserDto,
  movie: SimpleMovieDto,
  scriptRating: number,
  ideaRating: number,
  visualsEditRating: number,
  musicRating: number,
  actingRating: number,
}

export interface SimplerUserDto {
  id: number,
  createdAt: Date,
  updatedAt: Date,
  name: string,
  avatar: string,
}


export interface ReviewProps {
  id: number,
  userId: number,
  text: string,
  averageRating: number,
  userName: string,
  mutate: KeyedMutator<any>,
}


export interface MovieCardProps {
  id: number,
  name: string,
  poster: string,
  duration: number,
  mode: MovieCardMode
}
