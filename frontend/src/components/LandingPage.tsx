import useSWR from 'swr';
import Header from './Header';
import MovieCard from './MovieCard';
import { MovieCardMode, MovieDto } from '../models/types';
import LoadingPage from './LoadingPage';
import ErrorPage from './ErrorPage';

export const LandingPage = () => {
  const { data, error } = useSWR<MovieDto[]>('movies');

  if (error) return <ErrorPage />;
  if (!data) return <LoadingPage />;

  return (
    <div className="h-screen">
      <Header/>
      <main className="flex flex-wrap p-20">
        {data.map((movie: any) => <MovieCard key={movie.id} {...movie} mode={MovieCardMode.LandingPage}/>)}
      </main>
    </div>
  );
};

export default LandingPage;
