import Header from './Header';

export const PageNotFound = () => {
  return (
    <>
      <Header/>
      <div className="flex h-[90vh] justify-center">
        <span className="mt-10 text-3xl">
          <b>Page not found</b>
        </span>
      </div>
    </>
  );
};

export default PageNotFound;
