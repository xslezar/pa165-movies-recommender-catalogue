import Header from './Header';

export const ErrorPage = () => {
  return (
    <div>
      <Header/>
    <div>
      An error occurred.
  </div>
  </div>
);
};

export default ErrorPage;
