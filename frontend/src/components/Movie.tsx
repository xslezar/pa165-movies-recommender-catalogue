import Header from './Header';
import MovieCard from './MovieCard';
import { MovieCardMode, MovieDto, ReviewDto } from '../models/types';
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { useParams } from 'react-router-dom';
import instance from '../models/axios';
import useSWR from 'swr';
import { getToken, getUser } from '../services/auth';
import formatDuration from '../services/formatDuration';
import Review from './Review';
import LoadingPage from './LoadingPage';
import ErrorPage from './ErrorPage';

function getAverageRating(review: any) {
  return (review.actingRating + review.ideaRating + review.scriptRating + review.musicRating + review.visualsEditRating) / 5;
}

export const Movie = () => {
  const [ showReviews, setShowReviews ] = useState<boolean>(false);
  const { register, handleSubmit, getValues, watch } = useForm();
  watch();

  const sendReview = async (data: any) => {
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': getToken()
    };
    const reviewData = {
      ...data,
      movieId: id,
      userId: getUser()!.sub
    };
    await instance.post('reviews', reviewData, { headers });
    await mutateReviews();
    alert('Review created');
  };

  const { id } = useParams();
  const { data: movie, error: movieError } = useSWR<MovieDto>(`movies/${id}`);
  const { data: reviews, error: reviewError, mutate: mutateReviews } = useSWR<ReviewDto[]>(`reviews/search/${id}`);
  const { data: recommended, error: recommendedError } = useSWR<MovieDto[]>(`movies/${id}/recommended`);

  if (movieError || reviewError || recommendedError) return <ErrorPage />;
  if (!movie || !reviews || !recommended) return <LoadingPage />;

  const recommendedMovies = recommended.slice(0, 5);

  return (
    <div>
      <Header/>
      <div className="grid grid-cols-3 h-[90vh]">
        <div className="flex flex-col col-span-2 h-full">
          <div className="flex flex-row p-4 border-solid border-r-4 border-slate-900 bg-slate-300 h-full w-full">
            <div className="p-4 flex-shrink-0 h-full w-2/5">
              <img className="h-full w-full" src={movie.poster} alt="Poster"/>
            </div>
            <div className="flex flex-col p-4 w-full">
              <p className="text-2xl font-bold">{movie.name}</p>
              {movie.genres.length > 0 &&
                  <p><b>Genres: </b>
                    {movie.genres.slice(0, -1).map((genre: any) => `${genre}, `)}
                    {movie.genres.at(-1)}
                  </p>
              }
              <p><b>Duration: </b>{formatDuration(movie.duration)}</p>
              <p>{movie.description}</p>
              <p className="text-xl mt-auto"><b>Director: </b>{movie.director.name}</p>
              {movie.actors.length > 0 &&
                (<p className="text-xl"><b>Actors: </b>
                  {movie.actors.slice(0, -1).map((actor: any) => `${actor.name}, `)}
                  {`${movie.actors.at(-1)!.name}`}
                </p>)
              }
              <button className="w-max p-4 self-center bg-blue-600 border-2 rounded-3xl border-slate-900"
                      onClick={() => setShowReviews((prevState => !prevState))}>
                {showReviews ? 'Show recommended movies' : 'Show reviews'}
              </button>
            </div>
          </div>

        </div>
        <div className={`h-full ${showReviews && 'hidden'}`}>
          <p className="text-2xl text-center font-bold">Recommended movies</p>
          <div className="flex flex-row flex-wrap p-4">
            {recommendedMovies.map((movie: any) => <MovieCard key={movie.id} {...movie}
                                                              mode={MovieCardMode.Recommend}/>)}
          </div>
        </div>
        <div className={`h-full flex flex-col overflow-hidden ${showReviews || 'hidden'}`}>
          <p className="text-2xl text-center font-bold">Reviews</p>
          <div className="overflow-y-scroll">
            {reviews.map((review: any) =>
              <Review averageRating={
                getAverageRating(review)}
                      id={review.id}
                      userId={+review.user.id}
                      text={review.text}
                      userName={review.user.name}
                      mutate={mutateReviews}
                      key={review.id}
              />
            )}
          </div>
          {getUser() &&
              <form
                  onSubmit={handleSubmit(sendReview)}
                  className="flex flex-col p-2 m-2 mt-auto bg-slate-300 border-4 rounded-3xl border-slate-900"
              >
                  <span className="text-2xl text-center font-bold">Write your review</span>
                  <label className="flex justify-between">
                      <span className="font-bold">Script: {getValues('scriptRating')}</span>
                      <input type="range" min={1} max={10} {...register('scriptRating', { required: true })}/>
                  </label>
                  <label className="flex justify-between">
                      <span className="font-bold">Idea: {getValues('ideaRating')}</span>
                      <input type="range" min={1} max={10} {...register('ideaRating', { required: true })}/>
                  </label>
                  <label className="flex justify-between">
                      <span className="font-bold">Visual edits: {getValues('visualsEditRating')}</span>
                      <input type="range" min={1} max={10} {...register('visualsEditRating', { required: true })}/>
                  </label>
                  <label className="flex justify-between">
                      <span className="font-bold">Music: {getValues('musicRating')}</span>
                      <input type="range" min={1} max={10} {...register('musicRating', { required: true })}/>
                  </label>
                  <label className="flex justify-between">
                      <span className="font-bold">Acting: {getValues('actingRating')}</span>
                      <input type="range" min={1} max={10} {...register('actingRating', { required: true })}/>
                  </label>
                  <label>
              <textarea
                  className="w-full border-2 border-slate-900"
                  {...register('text', { required: true, minLength: 1 })}
                  placeholder={'Write your review here.'}
              />
                  </label>
                  <button type="submit"
                          className="w-max p-2 self-center bg-blue-600 border-2 rounded-3xl border-slate-900">
                      Send review
                  </button>
              </form>
          }
        </div>

      </div>
    </div>
  );
};

export default Movie;
