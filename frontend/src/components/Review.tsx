import { getToken, getUser } from '../services/auth';
import instance from '../models/axios';
import { ReviewProps } from '../models/types';

export const Review = ({id, userId, text, averageRating, userName, mutate}: ReviewProps) => {
  const user = getUser();

  const deleteReview = async () => {
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': getToken()
    };
    await instance.delete(`reviews/${id}`, { headers });
    await mutate();
    alert('Review deleted');
  };

  return (
    <div className="w-full h-40 p-2">
      <div className="border-solid border-4 border-slate-900 bg-slate-300 w-full h-full p-2 ">
        <div className="flex justify-between font-bold">
          <p>Username: {userName}</p>
          {(user?.role === 'ADMIN' || user?.sub == userId) &&
            (<button onClick={deleteReview} className="border-solid border-2 rounded-lg border-slate-900 p-1 bg-red-600">
                Delete
              </button>
            )
          }
          <p>Average Rating: {averageRating}</p>
        </div>
        <p>{text}</p>
      </div>
    </div>
  )
};

export default Review;
