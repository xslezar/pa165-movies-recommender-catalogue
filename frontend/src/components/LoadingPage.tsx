import Header from './Header';

export const LoadingPage = () => {
  return (
    <div>
      <Header/>
      <div>
        Data is loading...
      </div>
    </div>
  );
};

export default LoadingPage;
