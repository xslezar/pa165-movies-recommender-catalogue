import { BrowserRouter, Route, Routes } from 'react-router-dom';
import LandingPage from './LandingPage';
import Movie from './Movie';
import PageNotFound from './PageNotFound';
import { Login } from './Login';
import { SWRConfig } from 'swr';
import fetcher from '../models/fetcher';

export const Pages = () => (
  <SWRConfig value={{ fetcher }}>
    <BrowserRouter>
      <Routes>
        <Route path="pa165" element={<LandingPage/>}/>
        <Route path="pa165/movie/:id" element={<Movie/>}/>
        <Route path="pa165/login" element={<Login/>}/>
        <Route path="*" element={<PageNotFound/>}/>
      </Routes>
    </BrowserRouter>
  </SWRConfig>
);

export default Pages;
