import instance from '../models/axios';
import jwtDecode from 'jwt-decode';
import { UserAuth } from '../models/types';

export const login = async (name: string, password: string) => {
  try {
    const response = await instance.post('login', {
      name, password
    });
    sessionStorage.setItem('user', JSON.stringify(response.data));
    return response;
  } catch (error) {
    return error;
  }
}

export const logout = () => {
  sessionStorage.removeItem('user');
}

export const getToken = (): string => {
  const user = sessionStorage.getItem('user');
  return user ? JSON.parse(user) : null;
}

export const getUser = (): UserAuth | null => {
  const user = sessionStorage.getItem('user');
  return user ? jwtDecode(JSON.parse(user)) : null;
}

