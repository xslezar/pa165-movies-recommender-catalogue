package cz.fi.muni.pa165.movierecommender.rest.api;

import cz.fi.muni.pa165.movierecommender.api.dto.ReviewDto;
import cz.fi.muni.pa165.movierecommender.api.dto.create.ReviewCreateDto;
import cz.fi.muni.pa165.movierecommender.api.dto.update.ReviewUpdateDto;
import cz.fi.muni.pa165.movierecommender.api.facade.ReviewFacade;
import cz.fi.muni.pa165.movierecommender.persistence.entity.User;
import cz.fi.muni.pa165.movierecommender.rest.core.RoutesHolder;
import cz.fi.muni.pa165.movierecommender.service.service.UserService;
import cz.fi.muni.pa165.movierecommender.service.service.exception.ForbiddenOperationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Maxim Svistunov
 */
@RestController
@RequestMapping(RoutesHolder.REVIEW_ROUTE)
public class ReviewControllerProvider implements ReviewController {
    private final ReviewFacade reviewFacade;
    private final UserService userService;

    @Autowired
    public ReviewControllerProvider(ReviewFacade reviewFacade, UserService userService) {
        this.reviewFacade = reviewFacade;
        this.userService = userService;
    }

    @GetMapping
    @ResponseBody
    public List<ReviewDto> findAll() {
        return reviewFacade.findAll();
    }

    @GetMapping("search/{movieId}")
    @ResponseBody
    public List<ReviewDto> findByMovie(@PathVariable Long movieId) {
        if (movieId == null) {
            throw new IllegalArgumentException("You need to specify movieId");
        }

        return reviewFacade.findByMovie(movieId);
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping
    @ResponseBody
    public ReviewDto createReview(@RequestBody ReviewCreateDto reviewCreateDto) {
        if (reviewCreateDto == null) throw new IllegalArgumentException("Create review body cannnot be null");

        return reviewFacade.create(reviewCreateDto);
    }

    @PreAuthorize("isAuthenticated()")
    @PatchMapping
    @ResponseBody
    public ReviewDto updateReview(@RequestBody ReviewUpdateDto reviewUpdateDto) {
        if (reviewUpdateDto == null) throw new IllegalArgumentException("Update review body cannot be null");

        final User user = userService.getAuthenticatedUser();
        final ReviewDto review = reviewFacade.findById(reviewUpdateDto.getId());
        final boolean isMyReview = review.getUser() != null && user.getId().equals(review.getUser().getId());

        if (user.isAdmin() || isMyReview) {
            return reviewFacade.update(reviewUpdateDto);
        } else {
            throw new ForbiddenOperationException("Cannot update not own review or no admin rights");
        }
    }

    @PreAuthorize("isAuthenticated()")
    @DeleteMapping("{id}")
    @ResponseBody
    public void deleteReview(@PathVariable Long id) {
        final User user = userService.getAuthenticatedUser();
        final ReviewDto review = reviewFacade.findById(id);
        final boolean isMyReview = review.getUser() != null && user.getId().equals(review.getUser().getId());

        if (user.isAdmin() || isMyReview) {
            reviewFacade.delete(id);
        } else {
            throw new ForbiddenOperationException("Cannot delete not own review or no admin rights");
        }
    }
}