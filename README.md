# PA165 Theta Project

*PA165 course at MUNI Spring 2022.*

# Instructions

## How to run the app

### With docker
- Make sure your docker application is running
- `docker-compose up --build` build both FE and BE and runs both of them
- Webpage will be available on http://localhost:8081/pa165

### Without docker

Start backend
- `mvn clean install` Compiles source
- `mvn -f rest/pom.xml spring-boot:run` Runs the app

Start frontend (In another terminal)

- `cd frontend` Change to folder with FE 
- `npm i` Install dependencies
- `npm run dev` Start the FE application

Webpage will be available on http://localhost:8081/pa165

### Users
- There are 3 sampled users, "admin" with password "admin" and ADMIN role. "pepa" with password "pepa" with BASIC_USER role, "karel" with password "karel" with BASIC_USER role.
- User not logged in can view movies, view recommended movies for a movie and view reviews for a movie.
- Basic user can also write a review and delete his own reviews.
- Admin can write reviews and delete any reviews, 

### Testing the app

**About features**
- All exposed enpoints provide all the functionality that is available for the app - both CRUD and Business functions
- The FE application provides authentication, recommendation, review creation and deletion, and browsing of movies

**Swagger**
- Go to http://localhost:8080/pa165/rest/swagger-ui.html
- You need to authenticate, use token `eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJjei5maS5tdW5pLnBhMTY1Lm1vdmllcmVjb21tZW5kZXIiLCJpYXQiOjE2NTMzMTM4MjEsInN1YiI6IjEiLCJleHAiOjE2NTM0MDAyMjEsInJvbGUiOiJBRE1JTiIsIm5hbWUiOiJhZG1pbiJ9.douDjKgiV02Ct8FgBrAYnazNjXsPd1iPjdYiS-xFjHk`
- For each method you will have an option to try it out
- If you wish to change user role, just change it via:
  1. Login Controller Provider -> POST with body of user credentials (eg. "karel" "karel" )
  2. Copy the returned authentication token value to the form provided after clicking "Authorize" button 
  3. You are logged as a different user! (Notice how myInfo() from  User Controller Provider returns different info)
  4. To switch just logout also via "Authorize" button and repeat from 1.

**Alternatively** 
- Login in as user admin

`curl -X POST "http://localhost:8080/pa165/rest/login" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"name\": \"admin\", \"password\": \"admin\"}"`

- Get all movies

`curl -X GET "http://localhost:8080/pa165/rest/movies" -H "accept: */*"`

- Get all reviews for movie with id 18 (The Shawshank Redemption)

`curl -X GET "http://localhost:8080/pa165/rest/reviews/search/18" -H "accept: */*"`

# About Project

- **Name**: Movie Recommender Catalogue
- **Technologies**: Java 17, Spring, Hibernate, Maven, React + Typescript
- **Developers**:
    - Maxim Svistunov @xsvistun
    - Martin Bartoš @xbartos5
    - Daniel Puchala @xpuchal1
    - Petr Slezar - _Project Leader_ @xslezar
- **Assigment**:
    - The web application is a catalogue of movies of different genres. Each movie has a title, description, film director, and list of actors, and image(s) from the main playbill. Each movie is categorized in one or more genres. Only the administrator can create, remove and update the list of movies. Users can rate the movies according to different criteria (e.g how novel are the ideas of the movie, their final score, etc…). The main feature of the system is that users can pick one movie and get the list of similar movies and / or movies that were liked the most by other users watching the same movie (no need of complex algorithms, some simple recommendation is enough!).


# Project Description

Movie Recommender is a system for movies evidence and exploration, review writing and getting recommendations for films.

## Roles

System has two authorization roles - **Basic User** and **Administrator**.

- Basic user can browse movies in the database. Write and read reviews. Keep a collection of favorite movies and get recommendations for movies.
- Administrator has all possibilities of basic user, but can also add and edit movies as well as persons of film industry.

## Entities

- **DomainEntity** - abstract entity that is extended by every entity, provides access to `code-generated ID` (UUID) and
  audit attributes such as created and updated
- **Movie** - entity representing a movie in the system's database with its genres
- **Person** - entity representing a person of movie industry. In association with movie either a director or an actor
- **Team** - entity representing a composite review for a movie written by a user
- **User** - entity representing a user of the system as described in the previous section

# Diagrams
### Class 
![Class Diagram](https://gitlab.fi.muni.cz/xslezar/pa165-movies-recommender-catalogue/-/raw/milestone-1/documentation/MovieRecommenderEntity.jpg)
### Use case
![Use Case Diagram](https://gitlab.fi.muni.cz/xslezar/pa165-movies-recommender-catalogue/-/raw/milestone-1/documentation/movieRecommenderUseCase.png)
